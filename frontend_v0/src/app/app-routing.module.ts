import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EntrarComponent } from './Usuario/entrar/entrar.component';
import { MostrarComponent } from './Usuario/mostrar/mostrar.component';

const routes: Routes = [
  {path:'entrar', component:EntrarComponent},
  {path:'mostrar', component:MostrarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
