export class Usuario{
    id:String;
    tipo:String;
    nacimiento:Date;
    edad:number;
    telefono:String;
    correo:String;
}