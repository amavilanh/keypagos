import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { EntrarComponent } from './Usuario/entrar/entrar.component';
import { MostrarComponent } from './Usuario/mostrar/mostrar.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'keypago';

  constructor(private router:Router){}
    Entrar(){
      this.router.navigate(["entrar"]);
    }
    Mostrar(){
      this.router.navigate(["mostrar"]);
    }

  }


