import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import{ServiceService}from '../../Service/service.service';
import { Usuario } from 'src/app/Modelo/Usuario';

@Component({
  selector: 'app-entrar',
  templateUrl: './entrar.component.html',
  styleUrls: ['./entrar.component.css']
})
export class EntrarComponent implements OnInit{
  usuarios:Usuario[];
  constructor(private service:ServiceService, private router:Router){}

  ngOnInit(){
    this.service.getUsuarios()
    .subscribe(data=>{
      this.usuarios=data;
    })
  }
}
