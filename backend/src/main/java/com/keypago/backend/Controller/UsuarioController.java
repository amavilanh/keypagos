/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.keypago.backend.Controller;
import com.keypago.backend.models.Usuario;
import com.keypago.backend.Service.UsuarioService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andres
 */
@RestController
@RequestMapping("/usuario")
@CrossOrigin("http://localhost:4200")
public class UsuarioController {
    
    @Autowired
    private UsuarioService UsuarioService;
    
    @PostMapping(value="/guardar")
    public ResponseEntity<Usuario> agregar(@RequestBody Usuario usuario){
        
        Usuario obj = UsuarioService.save(usuario);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @DeleteMapping(value="/borrar/{id}")
    public ResponseEntity<Usuario> eliminar(@PathVariable String id){
        Usuario obj = UsuarioService.get(id);
        if(obj!=null)
            UsuarioService.delete(id);
        else
            return new ResponseEntity<Usuario>(HttpStatus.NO_CONTENT);
        return new ResponseEntity<Usuario>(obj, HttpStatus.OK);
    }
    
    @PutMapping(value="/list/{id}")
    public ResponseEntity<Usuario> editar(@RequestBody Usuario usuario){
        Usuario obj= UsuarioService.get(usuario.getId());
        if(obj!=null){
            obj.setCorreo(usuario.getCorreo());
            obj.setTipo(usuario.getTipo());
            obj.setEdad(usuario.getEdad());
            obj.setNacimiento(usuario.getNacimiento());
            obj.setTelefono(usuario.getTelefono());
            UsuarioService.save(obj);
        }
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @GetMapping("/list")
    public List<Usuario> VerTodo(){
        return UsuarioService.getAll();
    }
    
    @GetMapping("/list/{id}")
    public Usuario VerporCodigo(@PathVariable String id){
        return UsuarioService.get(id);
    }
    
    @GetMapping("/login")
    public boolean Login(String id, String password){
        Usuario obj= UsuarioService.get(id);
        if(obj!=null && obj.getPassword().equals(password))
                return true;
        else
            return false;
    }
    
    @GetMapping("/admin")
    public boolean Admin(String id){
        Usuario obj= UsuarioService.get(id);
        if(obj!=null && obj.isAdmin())
                return true;
        else
            return false;
    }
    
    
}
