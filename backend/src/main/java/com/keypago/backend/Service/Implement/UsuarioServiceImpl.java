/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.keypago.backend.Service.Implement;

import com.keypago.backend.Commons.GenericServiceImpl;
import com.keypago.backend.Dao.UsuarioDao;
import com.keypago.backend.Service.UsuarioService;
import com.keypago.backend.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author Andres
 */
@Service
public class UsuarioServiceImpl extends GenericServiceImpl<Usuario, String> implements UsuarioService {
    
    @Autowired
    private UsuarioDao UsuarioDao;

    @Override
    public CrudRepository<Usuario, String> getDao() {
        return UsuarioDao;
    }
    
}
