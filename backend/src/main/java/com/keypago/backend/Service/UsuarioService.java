/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.keypago.backend.Service;

import com.keypago.backend.Commons.GenericService;
import com.keypago.backend.models.Usuario;

/**
 *
 * @author Andres
 */
public interface UsuarioService extends GenericService<Usuario, String> {
    
}
