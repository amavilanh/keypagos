/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.keypago.backend.models;

import java.util.Date;
import org.springframework.data.annotation.Id;

/**
 *
 * @author Andres
 */
public class Usuario {
    
    @Id
    private String id;
    private String tipo;
    private Date nacimiento;
    private int edad;
    private String telefono;
    private String correo;
    private String password;
    private boolean admin;

    public Usuario() {
    }

    public Usuario(String id, String tipo, Date nacimiento, int edad, String telefono, String correo, String password, boolean admin) {
        this.id = id;
        this.tipo = tipo;
        this.nacimiento = nacimiento;
        this.edad = edad;
        this.telefono = telefono;
        this.correo = correo;
        this.password= password;
        this.admin=admin;
    }

    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        boolean flagmayus = false, flagnum=false, flagespec=false;
        if(password.length()>=8 ){
            for(int i=0; i<password.length();i++){
                if(Character.isUpperCase(password.charAt(i)))
                    flagmayus=true;
                if(Character.isDigit(password.charAt(i)))
                    flagnum=true;
                if(!Character.isLetterOrDigit(password.charAt(i)))
                    flagespec=true;
            }
            if(flagmayus && flagnum && flagespec)
                this.password = password;
            //Para encriptar usar clsse DigestUtils de Commons Codec.... queda pendiente, eso y los mensajes de contraseña invalida
        }
 
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
    
    
    
}
