import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../service/api/api.service';
import { Usuario } from 'src/app/models/Usuario';
import { Router } from '@angular/router';
import { NgLocalization } from '@angular/common';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  usuarios:Usuario[];

  constructor(private api:ApiService, private router:Router){}

  ngOnInit(){
    this.api.getUsuarios()
    .subscribe(data=>{
      this.usuarios=data;
    })
  }

  regresar(){
    this.router.navigate(['userdata']);
  }

  editar(id:any){
    this.router.navigate(['datos',id])
  }

  eliminar(id:any){
    this.api.deleteUsuario(id).subscribe(data =>{
      location.reload();
      console.log(data)
    })
  }

  agregar(){

  }

}
