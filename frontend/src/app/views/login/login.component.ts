import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {ApiService} from '../../service/api/api.service';
import {LoginInterface} from '../../models/login.interface';
import {Router} from '@angular/router'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm= new FormGroup({
    usuario : new FormControl('', Validators.required),
    password : new FormControl('',Validators.required)
  })

  constructor(private api:ApiService, private router:Router){}

  errorStatus:boolean = false;
  errorMsj:any = "";

  ngOnInit():void{
    this.checksession();
  }

  onLogin(){
    this.api.toLogin(this.loginForm).subscribe(data =>{
      if(data){
        localStorage.setItem('session', 'true');
        this.router.navigate(['userdata']);
      }else{
        this.errorStatus=true;
        this.errorMsj = "El usuario o la contraseña son incorrectos"
      }
    });
    
  }

  checksession(){
    if(localStorage.getItem('session')== 'true'){
      this.router.navigate(['userdata']);
    }
  }

}
