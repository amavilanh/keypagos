import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../service/api/api.service';
import { Usuario } from 'src/app/models/Usuario';
import { ActivatedRoute, Router } from '@angular/router';
import {FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.component.html',
  styleUrls: ['./datos.component.css']
})
export class DatosComponent implements OnInit {

  constructor(private activerouter:ActivatedRoute, private router:Router, private api:ApiService){}

  usuario:Usuario;
  datauserForm= new FormGroup<any>({
    id : new FormControl({value:'', disabled: true}, Validators.required),
    tipo : new FormControl('',Validators.required),
    nacimiento : new FormControl('',Validators.required),
    telefono : new FormControl('',Validators.required),
    correo : new FormControl('',Validators.required)
  })

  ngOnInit(): void {
    let iduser = this.activerouter.snapshot.paramMap.get('id');
    this.api.getUsuario(iduser).subscribe(data =>{
      this.usuario=data;
      this.datauserForm.setValue({
        'id' : this.usuario.id,
        'tipo' : this.usuario.tipo,
        'nacimiento' : this.usuario.nacimiento,
        'telefono' : this.usuario.telefono,
        'correo' : this.usuario.correo
      });
      //console.log(this.datauserForm.value);
    })
  }

  postForm(form:Usuario){

    this.api.putUsuario(form).subscribe(data =>{
      console.log(data);
    })
  }

  regresar(){
    this.router.navigate(['admin']);
  }

  // getToken(){}    Se deberia reenviar el JWT del localstorage a la API

}
