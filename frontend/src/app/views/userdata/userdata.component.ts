import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ApiService} from '../../service/api/api.service';
import { Usuario } from 'src/app/models/Usuario';
import { VirtualTimeScheduler } from 'rxjs';

@Component({
  selector: 'app-userdata',
  templateUrl: './userdata.component.html',
  styleUrls: ['./userdata.component.css']
})
export class UserdataComponent implements OnInit{

  usuario:Usuario;
  admin:boolean;

  constructor(private api:ApiService, private router:Router){}

  identifier:any = localStorage.getItem('identifier');
  ngOnInit(){
    this.api.getUsuario(this.identifier).subscribe(data =>{
      this.usuario=data;
    });
    this.api.isAdmin(this.identifier).subscribe(data2 =>{
      this.admin=data2;
    });
  }

  salir(){
    localStorage.clear();
    this.router.navigate(['login']);
  }

  administrar(){
    this.router.navigate(['admin']);
  }

  
}
