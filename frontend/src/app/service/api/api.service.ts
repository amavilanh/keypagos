import { Injectable } from '@angular/core';
import {LoginInterface} from '../../models/login.interface';
import {ResponseInterface} from '../../models/response.interface';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { Usuario } from 'src/app/models/Usuario';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url:String = "http://localhost:8080/usuario/";

  constructor(private http:HttpClient) { }

  toLogin(form:FormGroup){
    let link = this.url + "login?id=" + form.value.usuario + "&password=" + form.value.password;
    localStorage.setItem('identifier', form.value.usuario);
    return this.http.get(link);
  }

  
   getUsuario(id:any){
    let link = this.url + "list/" + id;
    return this.http.get<Usuario>(link);
  }
  
/*
  getUsuario(id:any):Observable<Usuario>{
    let link = this.url + "list/" + id;
    return this.http.get<Usuario>(link);
  }
  */

  getUsuarios(){
    let link = this.url + "list/";
    return this.http.get<Usuario[]>(link);
  }

  isAdmin(ide:string){
    let link = this.url + "admin?id=" + ide;
    return this.http.get<boolean>(link);
  }

  putUsuario(form:Usuario){
    let link = this.url + "list/" + form.id;
    return this.http.put(link, form);
  }

  deleteUsuario(ide:string){
    let link = this.url + "borrar/" + ide;
    let Options = {
      headers: new HttpHeaders({
        'Conten-type': 'application/json'
      }),
      body:ide
    }
    return this.http.delete(link, Options);
  }
  //o podria haber llamado la url http://localhost:8080/usuario/borrar/1020804107?id=1020804107 y ya....

  
}
