import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './views/login/login.component';
import { UserdataComponent } from './views/userdata/userdata.component';
import {AdminComponent} from './views/admin/admin.component'
import {DatosComponent} from './views/datos/datos.component'

const routes: Routes = [
  {path:'', redirectTo:'login', pathMatch:'full'},
  {path:'login', component:LoginComponent },
  {path:'userdata', component:UserdataComponent},
  {path:'admin', component:AdminComponent},
  {path:'datos/:id', component:DatosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponets = [LoginComponent, UserdataComponent, AdminComponent, DatosComponent]
