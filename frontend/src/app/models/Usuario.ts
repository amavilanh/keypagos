export class Usuario{
    id:string;
    tipo:string;
    nacimiento:Date;
    edad:number;
    telefono:string;
    correo:string;
}